package br.eud.up;

import java.net.InetSocketAddress;
import javax.ws.rs.ext.RuntimeDelegate;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

@SuppressWarnings("restriction")
public class Servidor {	
	public static void main(String[] args) throws Exception {
		
		Provedor pv = new Provedor();
		RuntimeDelegate rd = RuntimeDelegate.getInstance();
	    HttpHandler ep = rd.createEndpoint(pv, HttpHandler.class);
	    InetSocketAddress isa = new InetSocketAddress(9090);
	    HttpServer server = HttpServer.create(isa, 0);
	    server.createContext("/", ep); 
	    server.start();
	    System.out.println("Web Service JAX-RS rodando!");
	
	}
}