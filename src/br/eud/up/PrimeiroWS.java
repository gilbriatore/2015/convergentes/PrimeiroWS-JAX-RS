package br.eud.up;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/ws")
public class PrimeiroWS {
	
	@GET @Produces(MediaType.TEXT_PLAIN)
	public String get(){
		return "Primeiro Web Services JAX-RS, plain/text";
	}
	
	@GET @Produces(MediaType.TEXT_HTML)
	public String getHTML(){
		return "<h1>Primeiro Web Services JAX-RS, html</h1>";
	}
	
	@GET @Produces(MediaType.TEXT_XML)
	public String getXML(){
		return "<text>Primeiro Web Services JAX-RS, text/xml</text>";
	}
	
	@GET @Produces(MediaType.APPLICATION_JSON)
	public String getJSON(){
	return "{ \"text\" : \"Primeiro Web Services JAX-RS, application/json\"}";
	}
}